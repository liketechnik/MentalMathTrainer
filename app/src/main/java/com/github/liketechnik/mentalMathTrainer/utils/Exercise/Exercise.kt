package com.github.liketechnik.mentalMathTrainer.utils.Exercise

import com.github.liketechnik.mentalMathTrainer.newTrainingSession.NegativeNumbersButtonState
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.util.*

/**
 * @author Florian Warzecha
 * @version 1.0
 * @date 11 of June 2017
 */
interface Exercise : Serializable {
    companion object {
        const val serialVersionUID: Long = 3L
    }

    val result: Int
    val firstPart: Int
    val secondPart: Int
    val maxValue: Int
    var userInput: Int
    val type: String
    var skipped: Boolean

    @Throws(IOException::class)
    fun writeObject(out: ObjectOutputStream)

    @Throws(IOException::class, ClassNotFoundException::class)
    fun readObject(input: ObjectInputStream)

    val solved: Boolean
        get() = result == userInput

    fun getRandomInt(random: Random, maxValue: Int, negativeNumbersState: Int): Int {
        var tmp = 0
        while (tmp <= 0) {
            tmp = random.nextInt(maxValue)
        }
        if (negativeNumbersState == NegativeNumbersButtonState.MIXED.ordinal) {
            if (random.nextBoolean()) {
                tmp *= -1
            }
        } else if (negativeNumbersState == NegativeNumbersButtonState.ONLY.ordinal) {
            tmp *= -1
        }
        return tmp
    }
}

enum class ExerciseTypes {
    ADDITION
}