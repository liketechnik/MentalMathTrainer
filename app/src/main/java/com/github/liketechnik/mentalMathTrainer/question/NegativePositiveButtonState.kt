package com.github.liketechnik.mentalMathTrainer.question

import android.app.Activity
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.utils.ButtonState

/**
 * @author liketechnik
 * @version 1.0
 * @date 21 of August 2017
 */
enum class NegativePositiveButtonState : ButtonState {

    POSITIVE() {
        override fun getText(activity: Activity): String {
            return activity.getString(R.string.addition_symbol)
        }
    },
    NEGATIVE() {
        override fun getText(activity: Activity): String {
            return activity.getString(R.string.subtraction_symbol)
        }
    };
}