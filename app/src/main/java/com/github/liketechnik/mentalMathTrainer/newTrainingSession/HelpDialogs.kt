package com.github.liketechnik.mentalMathTrainer.newTrainingSession


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.github.liketechnik.mentalMathTrainer.R

/**
 * @author liketechnik
 * @version 1.0
 * @date 20 of August 2017
 */

/** Builder for the help dialog about the maximumNumberOfExercises EditText  */
class numberOfExercisesHelpDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.number_of_exercises_help)
                .setTitle(R.string.number_of_exercises_help_dialog_heading)
                .setPositiveButton(R.string.ok_button) { dialog, id ->
                    // Do nothing, user knows what he wanted to know
                }

        return builder.create()
    }
}

/** Builder for the help dialog about the addition on/off switch  */
class additionSwitchHelpDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.addition_switch_help)
                .setTitle(R.string.addition_switch_help_dialog_heading)
                .setPositiveButton(R.string.ok_button) { dialog, id ->
                    // Do nothing, user knows what he wanted to know
                }

        return builder.create()
    }
}

/** Builder for the help dialog about the maxAdditionValue field  */
class maxAdditionValueHelpDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.max_addition_value_help)
                .setTitle(R.string.max_addition_value_help_dialog_heading)
                .setPositiveButton(R.string.ok_button) { dialog, id ->
                    // Do nothing, user knows what he wnated to know
                }

        return builder.create()
    }
}

/** Builder for the help dialog about the negative numbers button  */
class negativeNumbersHelpDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.max_addition_value_help)
                .setTitle(R.string.max_addition_value_help_dialog_heading)
                .setPositiveButton(R.string.ok_button) { dialog, id ->
                    // Do nothing, user knows what he wnated to know
                }

        return builder.create()
    }
}
