package com.github.liketechnik.mentalMathTrainer.calculateExercises

import android.app.ProgressDialog
import android.os.AsyncTask
import android.support.v4.app.DialogFragment
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.TrainingSession
import com.github.liketechnik.mentalMathTrainer.utils.Exercise.Addition
import com.github.liketechnik.mentalMathTrainer.utils.Exercise.Exercise
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.MAX_ADDITION_VALUE
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.NUMBER_OF_EXERCISES
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.defaultMaxAdditionValue
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.defaultNumberOfExercises

/**
 * @author Florian Warzecha
 * @version 1.0
 * @date 11 of June 2017
 */
class CalculateExercises(private val progressDialog: ProgressDialog, val caller: TrainingSession) : AsyncTask<Unit, Int, List<Exercise>>() {

    override fun doInBackground(vararg p0: Unit?): List<Exercise>? {
        val exercises: MutableList<Exercise> = mutableListOf()

        val numberOfExercises = caller.intent.getIntExtra(NUMBER_OF_EXERCISES, defaultNumberOfExercises)
        for (i in 0..numberOfExercises) {
            exercises.add(Addition(caller.intent.getIntExtra(MAX_ADDITION_VALUE, defaultMaxAdditionValue), activity = caller, negativeNumbersState = caller.negativeNumbersButton.state))
            publishProgress((i.toFloat() / numberOfExercises.toFloat() * 100).toInt())
            if (isCancelled) break
        }
        return exercises
    }

    override fun onProgressUpdate(vararg progress: Int?) {
        progressDialog.setMessage("${progress[0]} %")
    }

    override fun onPostExecute(result: List<Exercise>) {
        progressDialog.hide()
        val startDialog = FinishedCalculationDialogFragment()
        startDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme)
        startDialog.exercises = result
        startDialog.caller = caller
        startDialog.show(caller.supportFragmentManager, "finishedCalculationDialogFragment")
    }
}
