package com.github.liketechnik.mentalMathTrainer.question

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.ResultOverview
import com.github.liketechnik.mentalMathTrainer.utils.Exercise.Exercise
import com.github.liketechnik.mentalMathTrainer.utils.StateButton
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.CURRENT_QUESTION_INDEX
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.NUMBER_OF_EXERCISES
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.QUESTION
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.defaultNumberOfExercises

class Question : AppCompatActivity() {

    lateinit var negativePositiveButton: StateButton
    lateinit var question: Exercise
    var currentQuestionIndex = -1
    var numberOfQuestions = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)

        currentQuestionIndex = intent.getIntExtra(CURRENT_QUESTION_INDEX, 0)
        numberOfQuestions = intent.getIntExtra(NUMBER_OF_EXERCISES, defaultNumberOfExercises)

        question = intent.getSerializableExtra(QUESTION + currentQuestionIndex) as Exercise

        this.title = getString(R.string.title_activity_question) + " " + (currentQuestionIndex + 1).toString() + " / " + numberOfQuestions.toString()
        val exercisePartOne = findViewById(R.id.exercise_part_one) as TextView
        exercisePartOne.text = question.firstPart.toString()
        val exercisePartTwo = findViewById(R.id.exercise_part_two) as TextView
        exercisePartTwo.text = question.secondPart.toString()
        val exerciseSymbol = findViewById(R.id.exercise_symbol) as TextView
        exerciseSymbol.setText(R.string.addition_symbol)

        negativePositiveButton = StateButton(NegativePositiveButtonState.POSITIVE.ordinal, findViewById(R.id.negative_positive_button) as Button, this,
                NegativePositiveButtonState.values())
    }

    fun onClickPositiveNegativeButton(view: View) {
        negativePositiveButton.toggle()
    }

    fun onSkipExercise(view: View) {
        question.skipped = true
        onProceed(view)
    }

    fun onNextQuestion(view: View) {
        (findViewById(R.id.result) as EditText).text.toString().setInputOrSkipped(question)
        onProceed(view)
    }

    private fun onProceed(view: View) {
        val activity: Intent = if (currentQuestionIndex < (numberOfQuestions - 1)) { // we have a next question ready, prepare next question activity for that
            Intent(this, Question::class.java)
        } else { // no questions, prepare overview of the users performance
            Intent(this, ResultOverview::class.java)
        }

        // transfer question values to new intent
        activity.putExtra(CURRENT_QUESTION_INDEX, currentQuestionIndex + 1)
        activity.putExtra(NUMBER_OF_EXERCISES, numberOfQuestions)
        for (i in 0 until numberOfQuestions) {
            activity.putExtra(QUESTION + i, this.intent.getSerializableExtra(QUESTION + i))
        }
        activity.putExtra(QUESTION + currentQuestionIndex, question)

        startActivity(activity)
    }

    private fun String.setInputOrSkipped(question: Exercise) {
        try {
            question.userInput = this.toInt()
            if (negativePositiveButton.state == NegativePositiveButtonState.NEGATIVE.ordinal) {
                question.userInput *= -1
            }
        } catch (e: NumberFormatException) {
            question.skipped = true
        }
    }
}
