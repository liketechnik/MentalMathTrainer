package com.github.liketechnik.mentalMathTrainer.utils

import android.app.Activity
import android.widget.Button


/**
 * @author liketechnik
 * @version 1.0
 * @date 20 of August 2017
 */
class StateButton(initState: Int, val button: Button, val activity: Activity, state: Array<out ButtonState>) {
    public var state: Int = initState
        set(value) {
            field = value
            setText()
        }

    private val states: Array<ButtonState?> = arrayOfNulls(state.size)

    init {
        for (stateToAdd in state) {
            states[stateToAdd.ordinal] = stateToAdd
        }

        setText()
    }

    public fun toggle() {
        state = if (state + 1 >= states.size) {
            0
        } else {
            state + 1
        }
        setText()
    }

    private fun setText() {
        button.text = states[state]?.getText(activity)
    }
}