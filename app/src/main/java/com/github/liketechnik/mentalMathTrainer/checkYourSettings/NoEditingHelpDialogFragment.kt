package com.github.liketechnik.mentalMathTrainer.checkYourSettings

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.newTrainingSession.NewTrainingSession
import com.github.liketechnik.mentalMathTrainer.utils.packTrainingSessionIntent


/**
 * @author liketechnik
 * @version 1.0
 * @date 24 of August 2017
 */
class NoEditingHelpDialogFragment : DialogFragment() {
    lateinit var checkYourSettings: CheckYourSettings

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.no_editing_help)
                .setTitle(R.string.no_editing_help_dialog_heading)
                .setPositiveButton(R.string.ok_button) { _, _ ->
                    // Do nothing, user knows what he wanted to know
                }
                .setNegativeButton(R.string.back_button) { _, _ ->
                    startActivity(packTrainingSessionIntent(checkYourSettings, NewTrainingSession::class.java))
                }
        return builder.create()
    }
}