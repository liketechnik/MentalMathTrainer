package com.github.liketechnik.mentalMathTrainer.newTrainingSession

import android.app.Activity
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.utils.ButtonState

/**
 * @author liketechnik
 * @version 1.0
 * @date 20 of August 2017
 */
enum class NegativeNumbersButtonState(private val stringResId: Int) : ButtonState {

    NONE(R.string.none),
    MIXED(R.string.mixed),
    ONLY(R.string.only);

    public override fun getText(activity: Activity): String {
        return activity.getString(stringResId)
    }
}

//public fun com.github.liketechnik.mentalMathTrainer.newTrainingSession.NegativeNumbersButtonState.getState(ordinal: Int): NegativeNumbersButtonState =
//        when (ordinal) {
//            NegativeNumbersButtonState.NONE.ordinal -> NegativeNumbersButtonState.NONE
//            NegativeNumbersButtonState.ONLY.ordinal -> NegativeNumbersButtonState.ONLY
//            NegativeNumbersButtonState.MIXED.ordinal -> NegativeNumbersButtonState.MIXED
//            else -> {
//                throw IllegalArgumentException("No matching state for ordinal $ordinal")
//            }
//        }