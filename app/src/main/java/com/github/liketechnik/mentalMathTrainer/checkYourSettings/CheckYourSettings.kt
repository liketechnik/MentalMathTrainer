package com.github.liketechnik.mentalMathTrainer.checkYourSettings

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.TrainingSession
import com.github.liketechnik.mentalMathTrainer.newTrainingSession.NegativeNumbersButtonState
import com.github.liketechnik.mentalMathTrainer.newTrainingSession.NewTrainingSession
import com.github.liketechnik.mentalMathTrainer.calculateExercises.CalculateExercises
import com.github.liketechnik.mentalMathTrainer.utils.StateButton
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.ADDITION
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.MAX_ADDITION_VALUE
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.NUMBER_OF_EXERCISES
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.defaultMaxAdditionValue
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.defaultNumberOfExercises
import com.github.liketechnik.mentalMathTrainer.utils.applyTrainingSessionIntent

class CheckYourSettings : TrainingSession() {

    override lateinit var negativeNumbersButton: StateButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_check_your_settings)

        negativeNumbersButton = StateButton(NegativeNumbersButtonState.NONE.ordinal, findViewById(R.id.negativeNumbersButton)
                as Button, this, NegativeNumbersButtonState.values())

        applyTrainingSessionIntent(this)
    }

    /** Called when the user edits the Maximum addition value  */
    fun onClickMaxAdditionValue(view: View) {
        val intent = intent

        val maxAdditionValue = intent.getIntExtra(MAX_ADDITION_VALUE,
                defaultMaxAdditionValue)

        val maxAdditionValueEditText = findViewById(R.id.max_addition_value) as EditText
        maxAdditionValueEditText.setText(maxAdditionValue.toString())

        val helpDialog = NoEditingHelpDialogFragment()
        helpDialog.checkYourSettings = this
        helpDialog.show(supportFragmentManager, "noEditingHelpDialogFragment")
    }

    fun onClickNegativeNumbers(view: View) {
        val helpDialog = NoEditingHelpDialogFragment()
        helpDialog.checkYourSettings = this
        helpDialog.show(supportFragmentManager, "noEditingHelpDialogFragment")
    }

    /** Called when the user edits the Number fo Exercises  */
    fun onClickNumberOfExercises(view: View) {
        val intent = intent

        val numberOfExercises = intent.getIntExtra(NUMBER_OF_EXERCISES,
                defaultNumberOfExercises)

        val numberOfExercisesEditText = findViewById(R.id.number_of_exercises) as EditText
        numberOfExercisesEditText.setText(numberOfExercises.toString())

        val helpDialog = NoEditingHelpDialogFragment()
        helpDialog.checkYourSettings = this
        helpDialog.show(supportFragmentManager, "noEditingHelpDialogFragment")
    }

    /** Called when the user clicks on the Addition switch  */
    fun onClickAdditionSwitch(view: View) {
        val intent = intent

        val addition = intent.getBooleanExtra(ADDITION, true)

        val additionSwitch = findViewById(R.id.addition_switch) as Switch
        additionSwitch.isChecked = addition

        val helpDialog = NoEditingHelpDialogFragment()
        helpDialog.checkYourSettings = this
        helpDialog.show(supportFragmentManager, "noEditingHelpDialogFragment")
    }

    fun onClickLetsGo(view: View) {
        val progress: ProgressDialog = ProgressDialog(this)
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progress.setTitle(R.string.exercise_calculation_dialog_heading)
        progress.setMessage("0 %")
        //progress.setCancelable(false)
        progress.show()
//        for (i in 0..100) {
//            Thread.sleep(500)
//            progress.setMessage("$i %")
//            progress.show()
//        }
        //test(progress).start()
        CalculateExercises(progress, this).execute()
    }
}
