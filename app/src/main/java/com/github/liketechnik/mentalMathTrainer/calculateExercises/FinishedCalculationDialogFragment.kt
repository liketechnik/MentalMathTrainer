package com.github.liketechnik.mentalMathTrainer.calculateExercises

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.TrainingSession
import com.github.liketechnik.mentalMathTrainer.question.Question
import com.github.liketechnik.mentalMathTrainer.utils.Exercise.Exercise
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants

/**
 * @author liketechnik
 * @version 1.0
 * @date 24 of August 2017
 */
class FinishedCalculationDialogFragment : DialogFragment() {
    lateinit var exercises: List<Exercise>
    lateinit var caller: TrainingSession

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(R.string.calculated_exercises)
                .setTitle(R.string.calculated_exercises_dialog_heading)
                .setPositiveButton(R.string.yes_button) { _, _ ->
                    val activity: Intent = Intent(caller, Question::class.java)
                    activity.putExtra(TrainingSessionConstants.CURRENT_QUESTION_INDEX, 0)
                    activity.putExtra(TrainingSessionConstants.NUMBER_OF_EXERCISES,
                            caller.intent.getIntExtra(TrainingSessionConstants.NUMBER_OF_EXERCISES, TrainingSessionConstants.defaultNumberOfExercises))
                    for (i in exercises.indices) {
                        activity.putExtra(TrainingSessionConstants.QUESTION + i, exercises[i])
                    }

                    startActivity(activity)
                }
                .setNegativeButton(R.string.no_button) { _, _ ->

                }
        return builder.create()
    }
}