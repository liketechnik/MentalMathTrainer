package com.github.liketechnik.mentalMathTrainer.newTrainingSession


import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.github.liketechnik.mentalMathTrainer.checkYourSettings.CheckYourSettings
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.TrainingSession
import com.github.liketechnik.mentalMathTrainer.utils.StateButton
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.maxNumberOfExercises
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.myMaxAdditionValue
import com.github.liketechnik.mentalMathTrainer.utils.applyTrainingSessionIntent
import com.github.liketechnik.mentalMathTrainer.utils.packTrainingSessionIntent


class NewTrainingSession : TrainingSession() {

    override lateinit var negativeNumbersButton: StateButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_training_session)


        negativeNumbersButton = StateButton(NegativeNumbersButtonState.NONE.ordinal, findViewById(R.id.negativeNumbersButton)
                as Button, this, NegativeNumbersButtonState.values())

        applyTrainingSessionIntent(this)
    }

    /** Called when the user uses the addition on/off switch  */
    fun onClickAdditionSwitch(view: View) {
        //* Makes sure the switch is on, because this is the only arithmetic operation available
        //* and displays a message to the user, saying the switch is not editable
        val context = applicationContext
        val text = "This option is not editable!"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(context, text, duration)
        toast.show()

        val addition_switch = findViewById(R.id.addition_switch) as Switch
        addition_switch.isChecked = true
    }

    /** Called when the user toggles the negative numbers button */
    fun onClickNegativeNumbersButton(view: View) {
        negativeNumbersButton.toggle()
    }

    /** Called when the user clicks the ? behind the addition on/off switch  */
    fun onClickAdditionSwitchHelp(view: View) {
        //* Displays a dialog with the requested help
        val helpDialog = additionSwitchHelpDialogFragment()
        helpDialog.show(supportFragmentManager, "additionSwitchHelpDialogFragment")
    }

    fun onClickNegativeNumbersButtonHelp(view: View) {
        //* Displays a dialog containing the requested help
        val helpDialog = negativeNumbersHelpDialogFragment()
        helpDialog.show(supportFragmentManager, "negativeNumbersButtonHelpDialogFragment")
    }

    /** Called when the user clicks the ? behind the maxAdditionValue EditText  */
    fun onClickMaxAdditionValueHelp(view: View) {
        //* Displays a dialog with the requested help/
        val helpDialog = maxAdditionValueHelpDialogFragment()
        helpDialog.show(supportFragmentManager, "maxAdditionValueHelpDialogFragment")
    }

    /** Called when the user enters a value into maxAdditionValue EditText  */
    fun onClickMaxAdditionValue(view: View) {
        //* Set my maximum value if the users value is bigger
        val editText = findViewById(R.id.max_addition_value) as EditText
        val currentValueString = editText.text.toString()

        var currentValue: Int
        try {
            currentValue = Integer.parseInt(currentValueString)
        } catch (e: NumberFormatException) {
            currentValue = 100
        }

        if (currentValue > myMaxAdditionValue) {
            editText.setText(myMaxAdditionValue.toString())
        }
    }

    /** Called when the user enters a value into NumberOfExercises EditText  */
    fun onClickNumberOfExercises(view: View) {
        //* Set my maximum value if the users value is bigger
        val editText = findViewById(R.id.number_of_exercises) as EditText
        val currentValueString = editText.text.toString()

        var currentValue: Int
        try {
            currentValue = Integer.parseInt(currentValueString)
        } catch (e: NumberFormatException) {
            currentValue = 10
        }

        if (currentValue > maxNumberOfExercises) {
            editText.setText(maxNumberOfExercises.toString())
        }
    }

    /** Called when the user click the ? behind the NumberOfExercises EditText  */
    fun onClickNumberOfExercisesHelp(view: View) {
        //* Displays a dialog with the requested help
        val helpDialog = numberOfExercisesHelpDialogFragment()
        helpDialog.show(supportFragmentManager, "numberOfExercisesHelpDialogFragment")
    }

    /** Called when the user clicks the "Go on!" button  */
    fun onClickNextActivity(view: View) {
        //* Starts the next Activity (Check Your settings)
        startActivity(packTrainingSessionIntent(this, CheckYourSettings::class.java))
    }
}
