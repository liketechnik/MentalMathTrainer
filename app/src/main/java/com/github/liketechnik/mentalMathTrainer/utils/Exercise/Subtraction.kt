package com.github.liketechnik.mentalMathTrainer.utils.Exercise

import android.support.v7.app.AppCompatActivity
import com.github.liketechnik.mentalMathTrainer.R
import com.github.liketechnik.mentalMathTrainer.newTrainingSession.NegativeNumbersButtonState
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.*

/**
 * @author Florian Warzecha
 * @version 1.0
 * @date 11 of June 2017
 */
class Subtraction constructor(override val maxValue: Int, override var userInput: Int = -1, negativeNumbersState: Int = NegativeNumbersButtonState.NONE.ordinal,
                                            activity: AppCompatActivity) : Exercise {
    override val type: String

    companion object {
        const val serialVersionUID: Long = 4L
    }

    @Throws(IOException::class)
    override fun writeObject(out: ObjectOutputStream) {
        out.putFields()
        out.writeFields()
    }

    @Throws(IOException::class, ClassNotFoundException::class)
    override fun readObject(input: ObjectInputStream) {
        input.readFields()
    }

    constructor(input: ObjectInputStream, activity: AppCompatActivity) : this(0, activity = activity) {
        readObject(input)
    }

    override val result: Int
    override val firstPart: Int
    override val secondPart: Int
    override var skipped: Boolean = false

    init {
        val random: Random = Random()
        secondPart = getRandomInt(random, maxValue, negativeNumbersState)
        firstPart = getRandomInt(random, maxValue, negativeNumbersState)
        result = secondPart - firstPart
        type = activity.getString(R.string.subtraction)
    }

    override fun toString(): String {
        return firstPart.toString() + " + " + secondPart.toString()
    }
}