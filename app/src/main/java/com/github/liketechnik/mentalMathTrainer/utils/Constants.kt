package com.github.liketechnik.mentalMathTrainer.utils

/**
 * @author liketechnik
 * @version 1.0
 * @date 11 of Juni 2017
 */
class TrainingSessionConstants {

    companion object {
        val ADDITION = "liketechnik.mentalmathtrainer.ADDITION"
        val NEGATIVE_NUMBERS = "liketechnik.mentalmathtrainer.NEGATIVE_NUMBERS"
        val MAX_ADDITION_VALUE = "liketechnik.mentalmathtrainer.MAX_ADDITION_VALUE"
        val NUMBER_OF_EXERCISES = "liketechnik.mentalmathtrainer.NUMBER_OF_EXERCISES"
        val FIRST_ACCESS = "com.github.liketechnik.mentalMathTrainer.FIRST_ACCESS"
        val CURRENT_QUESTION_INDEX = "com.github.liketechnik.mentalMathTrainer.CURRENT_QUESTION_INDEX"
        val QUESTIONS_ARRAY = "com.github.liketchnik.mentalMathTrainer.QUESTIONS_ARRAY"
        val QUESTION = "com.github.liketechnik.mentalMathTrainer.QUESTION#"

        val myMaxAdditionValue = 10000
        val maxNumberOfExercises = 1000
        val defaultMaxAdditionValue = 100
        val defaultNumberOfExercises = 10
    }
}