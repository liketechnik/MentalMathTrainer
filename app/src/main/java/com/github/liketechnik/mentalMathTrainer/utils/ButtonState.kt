package com.github.liketechnik.mentalMathTrainer.utils

import android.app.Activity

/**
 * @author liketechnik
 * @version 1.0
 * @date 20 of August 2017
 */
interface ButtonState {
    fun getText(activity: Activity): String

    val ordinal: Int
}