package com.github.liketechnik.mentalMathTrainer

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity

import android.view.View
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import com.github.liketechnik.mentalMathTrainer.utils.Exercise.Exercise
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants
import com.github.liketechnik.mentalMathTrainer.utils.TrainingSessionConstants.Companion.QUESTION

/**
 * @author Florian Warzecha
 * @version 1.0
 * @date 25 of June 2017
 */
class ResultOverview : AppCompatActivity() {

    var numberOfQuestions = -1
    var questions: Array<Exercise> = emptyArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_overview)

        numberOfQuestions = intent.getIntExtra(TrainingSessionConstants.NUMBER_OF_EXERCISES, TrainingSessionConstants.defaultNumberOfExercises)

        val questionsList: MutableList<Exercise> = mutableListOf()
        (0 until numberOfQuestions).mapTo(questionsList) { this.intent.getSerializableExtra(QUESTION + it) as Exercise }
        questions = questionsList.toTypedArray()


        val table: TableLayout = findViewById(R.id.result_table) as TableLayout
        for (i in questions.indices) {
            val row: TableRow = TableRow(this)
            val number: TextView = TextView(this)
            val correct: TextView = TextView(this)
            val type: TextView = TextView(this)

            number.text = (i + 1).toString()
            when {
                questions[i].skipped -> correct.text = getString(R.string.skipped)
                questions[i].solved -> correct.text = getString(R.string.correct_solution)
                else -> correct.text = getString(R.string.incorrect_solution)
            }
            type.text = questions[i].type

            row.addView(number)
            row.addView(correct)
            row.addView(type)
            row.setOnClickListener(OnClickListener(questions[i], i))

            table.addView(row)
        }
    }


    inner class OnClickListener(val exercise: Exercise, val number: Int) : View.OnClickListener {
        override fun onClick(view: View) {
            println(exercise.firstPart.toString() + "+" +  exercise.secondPart.toString())
            println(exercise.result)
            println(exercise.userInput)
            val detailDialog = ShowDetailsDialogFragment(exercise, number)
            detailDialog.show(supportFragmentManager, "")
        }
    }

    @SuppressLint("ValidFragment")
    inner class ShowDetailsDialogFragment(val exercise: Exercise, val number: Int) : DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val builder = AlertDialog.Builder(activity)

            val userResult: String = if (exercise.skipped) {
                getString(R.string.skipped)
            } else {
                exercise.userInput.toString()
            }

            builder.setMessage(getString(R.string.exercise_detail).replace("/exercise>", exercise.toString())
                    .replace("/user>", userResult).replace("/solution>", exercise.result.toString()))
                    .setTitle(getString(R.string.exercise_detail_dialog_heading).replace("/number>", (number + 1).toString()))
            return builder.create()
        }
    }
}

