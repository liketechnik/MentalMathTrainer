package com.github.liketechnik.mentalMathTrainer

import android.support.v7.app.AppCompatActivity
import com.github.liketechnik.mentalMathTrainer.utils.StateButton

/**
 * @author liketechnik
 * @version 1.0
 * @date 21 of August 2017
 */
abstract class TrainingSession : AppCompatActivity() {
    abstract var negativeNumbersButton: StateButton
}